<?php

function icgi_dp_setup()
{
    load_theme_textdomain( 'icgi_dp' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'main'    => __( 'Main Menu', 'icgi_dp' ),
        'social' => __( 'Social Links Menu', 'icgi_dp' ),
    ) );

    // Add theme support for Custom Logo.
    add_theme_support( 'custom-logo', array(
        'width'       => 250,
        'height'      => 250,
        'flex-width'  => true,
    ) );
}
add_action( 'after_setup_theme', 'icgi_dp_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function icgi_dp_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Sidebar', 'icgi_dp' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'icgi_dp' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 1', 'icgi_dp' ),
        'id'            => 'sidebar-2',
        'description'   => __( 'Add widgets here to appear in your footer.', 'icgi_dp' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 2', 'icgi_dp' ),
        'id'            => 'sidebar-3',
        'description'   => __( 'Add widgets here to appear in your footer.', 'icgi_dp' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'icgi_dp_widgets_init' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function twentyseventeen_javascript_detection() {
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyseventeen_javascript_detection', 0 );


/**
 * Customizing title tag values
 */
function icgi_dp_title( $title, $sep, $sepLoc )
{
    $title = '';
    if( is_home() )
    {
        $title .= get_bloginfo( 'name' );

        if( get_bloginfo( 'description' ) )
        {
            $title .= $sep;
            $title .= get_bloginfo( 'description' );
        }
    }
    elseif( is_page() || is_single() )
    {
        $title .= get_the_title();
        $title .= $sep;
        $title .= get_bloginfo( 'name' );
    } else 
    {
        $title .= get_bloginfo( 'name' );

        if( get_bloginfo( 'description' ) )
        {
            $title .= $sep;
            $title .= get_bloginfo( 'description' );
        }
    }
    return $title;
}
add_filter( 'wp_title', 'icgi_dp_title', 3, 10 );

function icgi_dp_enqueue()
{
    // Fonts
    $query_args = array(
        'family' => 'Open+Sans:400,600,700,800|Roboto:300,400,500,700,900',
        'subset' => 'latin,latin-ext',
    );
    wp_enqueue_style( 'google-font', add_query_arg( $query_args, '//fonts.googleapis.com/css' ), array(), null, 'all' );

    // Enqueue Styles
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css', array(), null, 'all' );
    wp_enqueue_style( 'icgi-dp-style', get_stylesheet_uri(), array(), null, 'all' );
    wp_enqueue_style( 'icgi-dp-media', get_template_directory_uri() . '/assets/css/media.css', array(), null, 'all' );

    // Enqueue Scripts
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), null, true );
}
add_action( 'wp_enqueue_scripts', 'icgi_dp_enqueue' );


include 'includes/functions-post_type.php';

// Disable Heartbeat API
function stop_heartbeat() { 
    wp_deregister_script('heartbeat'); 
}
add_action( 'init', 'stop_heartbeat', 1 );

// Disable Locking of edit posts/pages/custom post types
// add_filter( 'wp_check_post_lock_window', '__return_zero' );