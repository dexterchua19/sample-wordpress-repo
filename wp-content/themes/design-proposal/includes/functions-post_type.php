<?php

/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function icgi_dp_post_type() {

    $layoutLabels = array(
        'name'                => __( 'Layouts', 'text-domain' ),
        'singular_name'       => __( 'Layout', 'text-domain' ),
        'add_new'             => _x( 'Add New Layout', 'text-domain', 'text-domain' ),
        'add_new_item'        => __( 'Add New Layout', 'text-domain' ),
        'edit_item'           => __( 'Edit Layout', 'text-domain' ),
        'new_item'            => __( 'New Layout', 'text-domain' ),
        'view_item'           => __( 'View Layout', 'text-domain' ),
        'search_items'        => __( 'Search Layouts', 'text-domain' ),
        'not_found'           => __( 'No Layouts found', 'text-domain' ),
        'not_found_in_trash'  => __( 'No Layouts found in Trash', 'text-domain' ),
        'parent_item_colon'   => __( 'Parent Layout:', 'text-domain' ),
        'menu_name'           => __( 'Layouts', 'text-domain' ),
    );

    $layoutArgs = array(
        'labels'              => $layoutLabels,
        'hierarchical'        => false,
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,
        'menu_icon'           => null,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => array( 'slug' => _x( 'layouts', 'URL slug' ) ),
        'capability_type'     => 'post',
        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'trackbacks', 'revisions')
    );
    

    register_post_type( 'layouts', $layoutArgs );
}
add_action( 'init', 'icgi_dp_post_type' );
