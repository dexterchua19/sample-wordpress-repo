<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php wp_title( ' | ', true ); ?></title>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <div class="header">
        <!-- Logo -->
        <div class="logo">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <a href="<?php bloginfo( 'home' ); ?>"><img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/images/iconcept.png" alt="iConcept Global Advetising Inc., Design Proposal"></a>      
                    </div>
                </div>
            </div>
        </div>
        <!-- End Logo -->

        <!-- Banner -->
        <div class="banner" style="background: url(<?php bloginfo( 'template_directory' ); ?>/assets/images/image-banner.jpg) center center no-repeat transparent;">
            <h2>Subpage Design Proposal</h2>
        </div>
        <!-- End Banner -->
    </div>

    <div class="content-area">